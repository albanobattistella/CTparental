#!/bin/sh
groupadd -r sudo 
#gpasswd -a sysadmin sudo

{
echo '%sudo   ALL=(ALL:ALL) ALL'
} > /etc/sudoers.d/gsudo

## 
if [ $(grep -c "sudo" /etc/profile) -eq 0 ]; then
{
echo '
# For sudo users, ensure that /usr/local/sbin, /usr/sbin, and /sbin are in
# the $PATH.  Some means of connection dont add these by default (sshd comes
# to mind).
if [ "$(groups | grep -cE "( sudo )|( sudo$)|(sudo)")" -ge "1" ]; then
  echo $PATH | grep /usr/local/sbin 1> /dev/null 2> /dev/null
  if [ ! $? = 0 ]; then
    PATH=/usr/local/sbin:/usr/sbin:/sbin:$PATH
  fi
fi'
} >> /etc/profile
fi
