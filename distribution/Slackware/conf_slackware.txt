nano /etc/slackpkg/mirrors
slackpkg update gpg
slackpkg update
slackpkg install-new
slackpkg upgrade-all
slackpkg clean-system
lilo


# clavier azerty serveur x
{
echo '
Section "InputClass"
	Identifier "keyboard-all"
	MatchIsKeyboard "on"
	MatchDevicePath "/dev/input/event*"
	Driver "evdev"
	Option "XkbLayout" "fr"
	#Option "XkbVariant" ""
	Option "XkbOptions" "terminate:ctrl_alt_bksp"
EndSection
'
} > /etc/X11/xorg.conf.d/90-keyboard-layout-evdev.conf
